FROM openjdk:11.0.3 

RUN mkdir /opt/app
COPY target/helloworld-1.0.jar /opt/app
WORKDIR /opt/app

EXPOSE 9011
CMD ["java", "-jar", "/opt/app/helloworld-1.0.jar"]
